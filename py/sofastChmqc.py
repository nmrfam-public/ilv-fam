###################################################
# 
#  Load sofast C13-HMQC starting from Bruker standard
#  HSQCETF3GP parameter set.
#  The routing must be set properly in the standard
#  Bruker parameter set for this macro to work
#
#  Marco@NMRFAM - mtonelli@wisc.edu
###################################################

import math, os, shutil

###################################################
module_file="/fleckvieh/tonelli/topspin/protein/py/nmrfam2.py" 
NMRFAM = os.path.isfile(module_file)
if NMRFAM:
### USE NMRFAM MODULE TO SET P1, PLW1, O1
   execfile(module_file)
   [cmd0,p1,plw1,o1] = inputp1pl1()
else:
### IF MODULE FILE IS NOT FOUND,
### READ P1, PLW1 and O1 FROM CURRENT EXPERIMENT
   p1  = GETPAR("P 1")
   pl1 = GETPAR("PLdB 1")
   o1 = GETPAR("O1")
   input = INPUT_DIALOG("Input","Please update the 1H pulse width, power level & offset",["P1  =", "PldB1 =", "O1 ="],[p1,pl1,o1],["","",""],["1","1","1"])

   cmd0 = "getprosol 1H %s %s" % (input[0],input[1])
   p1   = float(input[0])
   pl1  = float(input[1])
   o1   = float(input[2])
###################################################

XCMD("rpar HSQCETF3GP all",WAIT_TILL_DONE)

PUTPAR("PULPROG","sofastChmqc")

PUTPAR("DIGMOD","digital")
XCMD("1 FnMODE States-TPPI")
PUTPAR("1 NUC1", "13C")

## EXECUTE GETPROSOL
XCMD(cmd0,WAIT_TILL_DONE)
SLEEP(3)
cmd  = "o1 %f" % (o1); XCMD(cmd)
PUTPAR('RG','128.0')

### SPECTRAL WIDTH AND CARRIER SETTING ###
PUTPAR("2 O1P", "4.70")
PUTPAR("2 SW", "16.00")		# H1 direct window  (~16ppm)
PUTPAR("DW", str(int(float(GETPAR("2 DW")))))	# Round up dwell time number

PUTPAR("O3P", "118.00")
PUTPAR("O4P", "4.70")

###################################################
if NMRFAM:
  [CH3_in_f,CH3_o1p] = getCH3()	# if exist, read C13 parameters from local prosol file
else:
  CH3_o1p = "16.0"
  CH3_in_f=str(int((1.0/(20.0*(float(GETPAR("SFO2"))))*1.0e+6)))
PUTPAR("1 O1P", CH3_o1p)
PUTPAR("O2P", CH3_o1p)
PUTPAR("1 IN_F", CH3_in_f)

PUTPAR("1 TD", "256")
PUTPAR("2 TD", "2048")


### ACQUISITION EXTRACTED FROM ASED ###
PUTPAR("D 1", "1.0")
PUTPAR("D 16", "0.0002")
PUTPAR("DS", "16")
PUTPAR("NS", "8")

PUTPAR("CNST 1", "0.5")		# 1H methyl offset (ppm)
PUTPAR("CNST 2", "4.0")		# 1H methyl bandwidth (ppm)
PUTPAR("CNST 4", "125")		# JCH methyl coupling constant (125)

## CHANNEL NUC 1 ###
PUTPAR("CNST 3", "90")		# PC9 pulse flip angle (90-120)
PUTPAR("SPNAM 25", "Pc9_4_90.1000")
PUTPAR("SPOAL 25", "0.5")
PUTPAR("SPNAM 26", "Reburp.1000")
PUTPAR("SPOAL 26", "0.5")

## CHANNEL NUC 2 ###
PUTPAR("CPDPRG 2", "waltz65")

## CHANNEL NUC 3 ###

## GRADIENT CHANNEL ###
## ECHOS
PUTPAR("GPNAM 1", "SMSQ10.100")
PUTPAR("GPNAM 2", "SMSQ10.100")
PUTPAR("GPZ 1", "13.0")
PUTPAR("GPZ 2", "41.0")
PUTPAR("P 16", "1000")

PUTPAR('AUNM', 'au_startpy')
PUTPAR('PYNM','zgpy')


########################################
## SET UP PROCESSING PARAMETERS
########################################
# ZERO FILLING
PUTPAR("2 SI", "2048")
PUTPAR("1 SI", "512")
# WINDOW FUNCTIONS
PUTPAR("2 WDW", "QSINE")
PUTPAR("1 WDW", "QSINE")
PUTPAR("2 SSB", "2")
PUTPAR("1 SSB", "2")
# PHASE
PUTPAR("1 PHC0","90.0")
PUTPAR("1 PHC1","-180.0")
PUTPAR("1 FCOR","1.0")
# TURN OF LINEAR PREDICTION
PUTPAR("1 ME_mod","no")
# SET REFERENCING CORRECTION TO ZERO
PUTPAR("2 SR","0.0")
PUTPAR("1 SR","0.0")
# SET WATER FILTER
PUTPAR("2 BC_mod","qfil")
PUTPAR("2 BCFW","0.1")

MSG("Experiment was loaded successfully")
