; 3D C,C,H  sofast C-HMQC NOESY sofast C-HMQC
;avance-version (22/06/29)
;
;Note: F1 is the NOE C13 indirect dimension
;
;uses PC9 selective excitation 90 pulses on methyl protons
;uses REBURP selective refocusing 180 pulses on methyl protons
;cnst1: H(C) excitation frequency (in ppm) (0.5ppm)
;cnst2: H(C) excitation band width (in ppm) (4.0ppm)
;
;with optional EBURP pulse to suppress strong signal from lipids (use -DLIPID_FLG)
;cnst14: bandwidth (in ppm) Ebpurp lipid suppression pulse (<1ppm)
;cnst13: offset (in ppm) largest lipid peak
;
;phase sensitive
;with decoupling during acquisition
;
;P.Schanda and B. Brutscher, J. Am. Chem. Soc. 127, 8014 (2005)
;
;Marco@NMRFAM - mtonelli@wisc.edu
;Disclaimer: This pulse program implementation was created by the 
;National Magnetic Resonance Facility at Madison (NMRFAM). Support 
;for its use is provided to users within NMRFAM. It is supplied �as is� 
;for your information, please use appropriate caution when employing 
;it outside NMRFAM. Neither NMRFAM nor University of Wisconsin-Madison 
;are liable for any physical or other damage incurred during the use 
;of this pulse program.
;
;$CLASS=HighRes
;$DIM=3D
;$TYPE=
;$SUBTYPE=
;$COMMENT=

prosol relations=<triple>

#include <Avance.incl>
#include <Grad.incl>
#include <Delay.incl>

"d11=30m"
"d12=20u"
"d13=4u"
"d26=1s/(cnst4*2)"	/* cnst4 = 125Hz (JCH) */

"in0=inf1/2"
"in10=inf2/2"

"d0=in0/2-p3*2/3.1415"
"d10=in10/2-p3*2/3.1415"

/*******************************************************************/
/*   calculation of shaped 1H pulse parameters                     */
/*******************************************************************/
;cnst1: H(C) excitation frequency (in ppm)
;cnst2: H(C) excitation band width (in ppm)
;cnst3: PC9 flip angle
;spnam25: set to Pc9_4_90.1000
;spnam26: set to Reburp.1000
;spnam28: set to Eburp2.1000
;spnam29: set to Eburp2tr.1000

/******************************************/
/*  PC9 (p41, sp25)   */
/******************************************/
"p41=7.2/(cnst2*bf1/1000000)" /*  PC9  pulse length  */
"spw25=plw1*(pow((cnst3/90.0)*(p1/p41)/0.125,2))" /* PC9  power level  */
"spoff25=bf1*(cnst1/1000000)-o1"  /*  PC9  offset */
"spoal25=0.5"

/******************************************/
/*  REBURP (p42, sp26)   */
/******************************************/
"p42=4.875/(cnst2*bf1/1000000)" /* REBURP pulse length  */
"spw26=plw1*(pow((p1*2/p42)/0.0798,2))"   /* REBURP power level  */
"spoff26=bf1*(cnst1/1000000)-o1" /* REBURP offset */
"spoal26=0.5"

/******************************************/
/*  EBURP & EBURP_TR  (p43, sp28, sp29)   */
/******************************************/
"p43=4.6/(cnst2*bf1/1000000)" /*  EBURP pulse length   */
"spw28=plw1*(pow((p1*1.04/p43)/0.06103,2))"   /* EBURP power level  */
"spw29=plw1*(pow((p1*1.04/p43)/0.06103,2))"   /* EBURP power level  */
"spoff28=bf1*(cnst1/1000000)-o1" /*  EBURP offset */
"spoal28=0"
"spoff29=bf1*(cnst1/1000000)-o1" /*  EBURP_REV offset */
"spoal29=1.0"

/*****************************************************/
/*  EBURP for supperssion of lipid peak (p14, sp14)  */
/*****************************************************/
;cnst14: bandwidth (ppm) Ebpurp lipid suppression pulse (<1ppm)
;cnst13: offset (ppm) largest lipid peak
;spnam14: set to Eburp2.1000
#ifdef LIPID_FLG
  "p14=4.952/(cnst14*bf1/1000000)"			/* Eburp2 pulse length */
  "spw14=plw1*(pow((p1*1.0/p14)/0.0610296,2))"		/* Eburp2 power level */
  "spoff14=0"						/* Eburp2 offset */
  "spoal14=0"
#endif	/* LIPID_FLG */
/*****************************************************/

/* FIRST HMQC DELAYS */
#ifdef EBURP_FLG
"DELTA3=d26-d13-p16-d16"		/* EBURP */
#else	/* EBURP */
"DELTA3=d26-d13-p16-d16-p41*0.513"	/* PC9 */
#endif	/* EBURP */
/* MIXING TIME DELAY */
"DELTA4=d8-p3-d13-p16-d16"
/* SECOND HMQC DELAYS */
"DELTA1=d26-d13-p16-d16-p41*0.52"	/* PC9 */
"DELTA2=p41*0.52-de-4u"


"acqt0=0"
baseopt_echo

aqseq 321

1 ze 
  d11 pl12:f2
2 d1 do:f2
3 d12 pl2:f2
  50u UNBLKGRAD

/**************************/
/* Lipid selective Eburp2 */
/**************************/
#ifdef LIPID_FLG
  10u fq=cnst13(bf ppm):f1   /* offset of strongest lipid signal in ppm */
  4u
  (p14:sp14 ph20):f1
  4u
  10u fq=0(sfo hz):f1 
#endif	/* LIPID_FLG */
/**************************/

  p16:gp4
  d16

/**************************************/
/*   First C-HMQC STARTS              */
/**************************************/
#ifdef EBURP_FLG
  (p43:sp28 ph1)    /* EBURP  */
#else	/* EBURP */
  (p41:sp25 ph1)    /*  PC9  */
#endif	/* EBURP */
 
  d13
  p16:gp3
  d16

if "td1 > 1"
 {
  (center (p42:sp26 ph2):f1 (DELTA3 p3 ph5 d0 d0 p3 ph6 DELTA3):f2 )
 }
else
 {
  (center (p42:sp26 ph2):f1 (DELTA3 p3 ph5 p3 ph6 DELTA3):f2 )
 }

  d13
  p16:gp3
  d16

#ifdef EBURP_FLG
  (p43:sp29 ph1)   /* EBURP_REV */
#else	/* EBURP */
  (p41:sp25 ph1):f1   /*  PC9  */
#endif	/* EBURP */
/**************************************/
/*   First C-HMQC END                 */
/**************************************/
/*   Mixing Time STARTS               */
/**************************************/
  DELTA4
  (p3 ph1):f2
  d13
  p16:gp2
  d16
/**************************************/
/*   Mixing Time ENDS                 */
/**************************************/
/*   Second C-HMQC STARTS             */
/**************************************/
  (p41:sp25 ph1):f1
  ;;(p43:sp28 ph1)    /* EBURP  */

  d13
  p16:gp1
  d16

if "td2 > 1"
 {
  (center (p42:sp26 ph2):f1 (DELTA1 p3 ph3 d10 d10 p3 ph4 DELTA1):f2 )
 }
else
 {
  (center (p42:sp26 ph2):f1 (DELTA1 p3 ph3 p3 ph4 DELTA1):f2 )
 }

  DELTA2	/* PC9 delay compensation */
  d13
  p16:gp1
  d16 pl12:f2
  4u BLKGRAD

  go=2 ph31 cpd2:f2 
  d1 do:f2 mc #0 to 2 
     F1PH(calph(ph5, +90), caldel(d0, +in0))
     F2PH(calph(ph3, +90), caldel(d10, +in10))

exit 
  
ph1=0
ph2=0 
ph3=0 2
ph4=0 0 0 0 2 2 2 2
ph5=0 0 2 2
ph6=0 0 0 0 0 0 0 0 2 2 2 2 2 2 2 2
ph31=0 2 2 0 2 0 0 2 2 0 0 2 0 2 2 0

ph20=0
ph21=1
ph22=2
ph23=3


;pl3 : f2 channel - power level for pulse (default)
;pl12: f2 channel-power level for CPD/BB decoupling
;
;p16: homospoil/gradient pulse                       [1 msec]
;p3: 13C -  90 degree high power pulse
;
;d0: incremented delay (3D) = in0/2
;d10: incremented delay (3D) = in10/2
;d1: relaxation delay
;d11: delay for disk I/O                             [30 msec]
;d12: delay for power switching                      [20 usec]
;d16: delay for homospoil/gradient recovery
;d26: 1/(2*JCH) delay - calculated from cnst4
;cnst4: = J(CH)
;
;inf1: 1/SW(C) = 2 * DW(C)
;inf2: 1/SW(C) = 2 * DC(C)
;in0: 1/ SW(C) = 2 * DW(C)
;in10: 1/ SW(C) = 2 * DW(C)
;ns: 4 * n
;ds: 16
;
;aq: <= 100 msec
;td1: number of experiments in F1
;td2: number of experiments in F2
;FnMODE: States-TPPI in F1
;FnMODE: States-TPPI in F2
;cpd2: decoupling according to sequence defined by cpdprg3: garp4.p61
;pcpd2: f3 channel - 90 degree pulse for decoupling sequence

;use gradient ratio:	NOT needed for this experiment

;for z-only gradients:
;gpz1: 53%   make high for water suppression
;gpz2: 29%
;gpz3: 19%
;gpz4: 13%

;use gradient files:   
;gpnam1: SMSQ10.100
;gpnam2: SMSQ10.100
;gpnam3: SMSQ10.100
;gpnam4: SMSQ10.100


                                          ;preprocessor-flags-start
;LIPID_FLG: use selective EBURP pulse to suppress unwanted signal from lipids
;           set cnst14 to bandwidth in ppm (1ppm or less)
;           set cnst13 to offset in ppm of largest lipid peak
;           set spnam14 shape name to Eburp2.1000
;           option -DLIPID_FLG (eda: ZGOPTNS)
                                          ;preprocessor-flags-end


;Processing

;PHC0(F1): 90
;PHC1(F1): -180
;FCOR(F1): 1



;$Id: sfhmqcf3gpph,v 1.10.6.2 2013/03/08 15:13:06 ber Exp $
